# template-post-forum
[Template](http://rikudourpg.forumeiros.com/t711-template#4679) para posts no [forum](http://rikudourpg.forumeiros.com/), inspirado no plugin do vim [goyo](https://github.com/junegunn/goyo.vim) com as cores do tema [onedark](https://github.com/jonathanchu/atom-one-dark-theme).

## vantagens
* Separação de ambiente dev (identações e linhas quebradas) do produtivo (minificado)
* Contador de palavras
* Agilidade com live reload do gulp

## setup
### Usando npm e gulp (recomendado)
#### Install
```
npm install # somente na primeira vez
```
#### Run
```
gulp
```
### Usando somente php
```
php -S localhost:8080 -t pagina
```

## alteração no template
* Modificar arquivo template.php;
* Acessar localhost:8080 para pré-visualizar/testar;

## criação de post
* Escrever em roleplay.txt;
* Acessar localhost:8080 para pré-visualizar;
* Acessar localhost:8080/build.php para obter o post minificado;

## atualização do folder pagina
> Não recomendo, mas caso queira atualizar o folder pagina é necessario seguir alguns procedimentos

* Excluir folder pagina;
* Baixar a pagina web com a pasta de imagens/scripts/estilos/etc;
* Renomear arquivo fora da pasta de ``algumacoisa.html`` para ``index.php``;
* Mover ``index.php`` e copiar ``build.php`` para folder pagina;
* Localizar região do conteúdo do post e substituir por ``<?php require '../config.php'; require TEMPLATE; ?>``;

## todo framework
- [x] Minificar arquivos
- [x] Live reload
- [ ] Facilitar a atualização do folder pagina
- [x] Modularizar a área de customização de post
- [x] Diferenciação de cores no roleplay.txt
- [x] adendos.txt/arsenal.txt transformado em lista
- [x] Status e descrições convertidos para o template
- [x] Contar palavras roleplay.txt e jogar dentro de adendos.txt
- [x] Descritivos e Status facilmente expansiveis
- [ ] Refatorar fichas, não é legal escrever html em string com php :thumbsdown:
- [ ] Conviver com [spoiler/]
