<?php
// O arquivo pode ser chamado multiplas vezes
if (!defined('ROOTPATH')) {
  define("ROOTPATH", dirname(__FILE__));
  define("DS", DIRECTORY_SEPARATOR);


  define("PAGINA", ROOTPATH . DS . 'pagina');
  define("INDEX", PAGINA . DS . 'index.php');
  define("BUILD", PAGINA . DS . 'build.php');


  define("SRC", ROOTPATH . DS . 'src');
  define("TEMPLATE", SRC . DS . 'template.php');
  define("ROLEPLAY", SRC . DS . 'roleplay.txt');
  define("LEGENDAS", SRC . DS . 'legendas.txt');
  define("ADENDOS", SRC . DS . 'adendos.txt');

  define("FICHAS", SRC . DS . 'fichas');
}
?>
