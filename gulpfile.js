const gulp = require('gulp');
const php = require('gulp-connect-php');
const bs = require('browser-sync').create();

function dev(){

  // inicia servidor php
  // ele pode ser inicializado separadamente por php -S localhost:8010 -t pagina
  php.server({base:'./pagina', port:8010, keepalive:true});

  // inicia browser async escutando :8010 e respondendo na :8080
  bs.init({
    proxy:"localhost:8010",
    port: 8080,
    baseDir: "./",
    open:true,
    notify:false,
    logLevel: "silent"
  });

  // gulp observa alterações em quaisquer arquivos php e txt da raiz do projeto
  // e recarrega o broser async
  gulp.watch(['src/*.php','src/*.txt','src/*/*/*.txt']).on('change', () => bs.reload());
};

// o comando "gulp" passa a ser chamar a função dev()
exports.default = dev


















/*
function defaultTask(cb) {
// place code for your default task here
php.server({base:'./pagina', port:8010, keepalive:true});
// cb();

browserSync.init({
proxy:"localhost:8010",
baseDir: "./",
open:true,
notify:false

});
}

exports.default = defaultTask
*/



// var gulp = require('gulp');
// var php = require('gulp-connect-php');
// var browserSync = require('browser-sync').create();
//
// gulp.task('php', function(){
//     php.server({base:'./', port:8010, keepalive:true});
// });
//
// gulp.task('browserSync',['php'], function() {
//     browserSync.init({
//         proxy:"localhost:8010",
//         baseDir: "./",
//         open:true,
//         notify:false
//     });
//   });
//
//   gulp.task('dev', [ 'browserSync'], function() {
//        gulp.watch('./*.php', browserSync.reload);
// });
